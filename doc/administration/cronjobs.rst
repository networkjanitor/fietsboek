Cronjobs
========

There are some maintenance jobs that should be done at regular intervals to
keep the instance running smoothly:

* Old, abandoned uploads should be deleted from the database to free up the
  space of the temporary GPX files.
* Tracks that are not present in the cache should have their cache entry
  populated by pre-calculating their length/uphill/downhill/… values. This
  makes accessing those values faster when a user requests the track's details.
* If enabled, heatmaps/tilehunts should be generated.

Fietsboek comes with a script to deal with those jobs: ``fietscron``. You can
run it like this::

    fietscron -c path_to_config.ini

The script is designed to be "smart", meaning that it splits the long tasks
(such as heatmap generation) over multiple runs, and it should work without
additional configuration.

You can use any mechanism you like to run this command at pre-defined intervals
(e.g. once an hour). You can find information about this in various sources,
depending on your system setup:

* https://wiki.archlinux.org/title/Systemd/Timers
* https://wiki.archlinux.org/title/Cron
* https://wiki.gentoo.org/wiki/Cron

For example, the classic *cron*-way to run ``fietscron`` once per hour would be
to add the following to your *crontab* (using ``crontab -e``)::

    0 * * * * /path/to/venv/bin/fietscron -c /path/to/settings.ini

