from fietsboek.views.browse import Stream


class TestStream:
    def test_write(self):
        stream = Stream()
        n = stream.write(b"foobar")
        assert n == 6

    def test_write_read(self):
        stream = Stream()
        stream.write(b"foo")
        stream.write(b"bar")
        assert stream.readall() == b"foobar"
