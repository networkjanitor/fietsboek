from playwright.sync_api import Page, expect


def test_homepage(page: Page):
    page.goto("/")
    assert "Welcome to Fietsboek!" in page.content()
    assert "Here you can …" in page.content()


def test_homepage_logged_in(page: Page, playwright_helper):
    playwright_helper.login()
    playwright_helper.add_track()

    page.goto("/")

    expect(page.locator("h1", has_text="Home")).to_be_visible()
    expect(page.locator("a", has_text="Another awesome track")).to_be_visible()


def test_homepage_collapse(page: Page, playwright_helper):
    playwright_helper.login()
    playwright_helper.add_track()

    page.goto("/")

    page.locator(".summary-toggler").nth(0).click()
    expect(page.locator("a", has_text="Another awesome track")).not_to_be_visible()
    expect(page.locator(".list-group-item-secondary", has_text="December")).not_to_be_visible()

    page.locator(".summary-toggler").nth(0).click()
    expect(page.locator("a", has_text="Another awesome track")).to_be_visible()
    expect(page.locator(".list-group-item-secondary", has_text="December")).to_be_visible()

    page.locator(".summary-toggler").nth(1).click()
    expect(page.locator("a", has_text="Another awesome track")).not_to_be_visible()
    expect(page.locator(".list-group-item-secondary", has_text="December")).to_be_visible()

    page.locator(".summary-toggler").nth(1).click()
    expect(page.locator("a", has_text="Another awesome track")).to_be_visible()
    expect(page.locator(".list-group-item-secondary", has_text="December")).to_be_visible()
