Language Packs
==============

Fietsboek allows language files to be distributed as third-party-packages, such
that you can localize Fietsboek without needing to add the files to the main
repository. This way, language packs can be updated independently of Fietsboek,
and packs can be provided for languages that are not "officially supported".

The basic concept behind language packs is the same as in normal
:doc:`localization <localize>`, except that the files are in a different Python
package. If you are familiar with the ``gettext`` machinery, you can simply
create a Python package that has a ``locale/`` folder in the same structure as
Fietsboek's.

In the following guide, we will create an example language pack for Dutch
(``"nl"``).

Preqrequisites
-------------------

In order to create a translation package, you need access to the source code of
Fietsboek and Babel_. The easiest way to get both is to clone the git
repository, and use a virtual environment to install babel:

.. code-block:: bash

    git clone https://gitlab.com/dunj3/fietsboek.git
    FB_PATH=$PWD/fietsboek/fietsboek
    virtualenv /tmp/babel
    /tmp/babel/pip install Babel
    BABEL=/tmp/babel/bin/pybabel


Creating a Package
------------------

Language packs are normal Python packages, as such, you can use any tool you'd
like to create a pack --- as long as you make sure to include the package data.
In our example, we will use Poetry_, as that is what Fietsboek itself uses.

To create a basic package, we will pick a path in which to create our pack:
:file:`~/fb-i18n-nl` and create the basic structure:

.. code-block:: bash

    PACK_PATH=~/fb-i18n-nl
    mkdir -p $PACK_PATH/fb_i18n_nl
    touch $PACK_PATH/fb_i18n_nl/__init__.py
    mkdir $PACK_PATH/fb_i18n_nl/locale

And the content of :file:`$PACK_PATH/pyproject.toml`:

.. code-block:: toml

    [tool.poetry]
    name = "fb-i18n-nl"
    version = "0.1.0"
    description = ""
    authors = ["Jan Modaal <jan.modaal@example.com>"]
    packages = [{include = "fb_i18n_nl"}]

    [tool.poetry.dependencies]
    python = "^3.7"

    [build-system]
    requires = ["poetry-core"]
    build-backend = "poetry.core.masonry.api"

.. note::

    You can also use ``poetry new`` to create the basic package, just make sure
    to remove the unnecessary files.

Initializing the Language
-------------------------

This is the same process as for built-in languages, except that you need to
adjust the paths:

.. code-block:: bash

    $BABEL init -d $PACK_PATH/fb_i18n_nl/locale -l nl -i $FB_PATH/locale/fietslog.pot

You can also copy over the English HTML files, which makes it easier to
translate them:

.. code-block:: bash

    cp -rv $FB_PATH/locale/en/html $PACK_PATH/fb_i18n_nl/locale/nl

Don't forget to adjust the :file:`locale/.../DISPLAY_NAME` file to give your
language pack the correct human-readable representation.

Translating & Compiling
-----------------------

Update the ``messages.po`` file in
:file:`$PACK_PATH/fb_i18n_nl/locale/nl/LC_MESSAGES/messages.po`, as well as the
HTML files in :file:`$PACK_PATH/fb_18n_nl/locale/nl/html`.

Once the messages have been translated, you can compile the resulting file:

.. code-block:: bash

    $BABEL compile -d $PACK_PATH/fb_i18n_nl/locale -l nl -i $PACK_PATH/fb_i18n_nl/locale/nl/LC_MESSAGES/messages.po

Installing the Language Pack
----------------------------

You can install the language pack like a normal Python package:

.. code-block:: bash

    pip install $PACK_PATH

And enable it in your settings:

.. code-block:: ini

    fietsboek.language_packs =
        fb_i18n_nl

    available_locales = en nl

Automation
----------

If you are fine with the following default values, then you can use the
provided :file:`justfile` with just_ to automate most of the boring processes:

* Poetry will be used
* The pack will be saved in :file:`language-packs/fietsboek-i18n-LOCALE`
* The module name will be ``fietsboek_i18n_LOCALE``

.. code-block:: bash

    just create-language-pack nl
    just update-language-pack nl
    just compile-language-pack nl

.. _Poetry: https://python-poetry.org/
.. _Babel: https://babel.pocoo.org/en/latest/index.html
.. _just: https://github.com/casey/just
