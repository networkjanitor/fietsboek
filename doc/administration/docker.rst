Container Setup
===============

Like most applications, Fietsboek can be installed in a Docker container. This
allows you to have Fietsboek isolated from the rest of the system, e.g. if you
need to run a newer Python version.

.. note::

    Fietsboek's ``Dockerfile`` has been tested with Podman_, but it should
    work just as well with Docker_.

    The commands in this article use ``docker`` for the sake of consistency,
    but it will work the same if you replace ``docker`` with ``podman`` (or use
    ``podman-docker``).

.. _Podman: https://podman.io/
.. _Docker: https://www.docker.com/

Building the Image
------------------

You can build the image by running ``docker build`` in the main directory. Note
that the build will use the currently checked out version of Fietsboek, so make
sure you are on the right version that you want to use::

    docker build -t fietsboek .

Container Layout
----------------

The container reserves three volumes:

* ``/fietsboek/database`` for the SQL database.
* ``/fietsboek/data`` for the track data.
* ``/fietsboek/pages`` for the custom pages.

Per default, the configuration file is expected at
``/fietsboek/fietsboek.ini``, and the `Gunicorn configuration`_ at
``/fietsboek/gunicorn.conf.py``.

.. _Gunicorn configuration: https://docs.gunicorn.org/en/latest/configure.html#configuration-file

Configuration
-------------

There are two ways to configure the Fietsboek instance in the container. You
can either mount a custom configuration file to ``/fietsboek/fietsboek.ini``,
or you can change common settings via environment variables. Note that you need
to choose one way, as mounting a custom configuration will disable the
environment variable parsing.

If you need to change a lot of settings, using a custom configuration file is
the prefered way. It follows the same syntax and settings as described in
:doc:`configuration`. Keep the container layout in mind when setting the data
directory and database location (or make sure that they are on other volumes).

An example command could look like this::

    docker run -v ./production.ini:/fietsboek/fietsboek.ini fietsboek

If you simply want to get a quick instance up and running, the method of
overwriting single configuration values via environment variables is quicker.
This way, you do not need to provide the full configuration file, but simply
the options that diverge from the default values::

    docker run -e REDIS_URL=redis://localhost fietsboek

The following environment variables are supported:

=============================== ===============================
Environment variable            Setting
=============================== ===============================
``REDIS_URL`` [#f1]_            ``redis.url``
``SQLALCHEMY_URL``              ``sqlalchemy.url``
``DATA_DIR``                    ``fietsboek.data_dir``
``SESSION_KEY`` [#f2]_          ``session_key``
``ENABLE_ACCOUNT_REGISTRATION`` ``enable_account_registration``
``DEFAULT_LOCALE_NAME``         ``pyramid.default_locale_name``
``EMAIL_FROM``                  ``email.from``
``EMAIL_SMTP_URL``              ``email.smtp_url``
``EMAIL_USERNAME``              ``email.username``
``EMAIL_PASSWORD``              ``email.password``
``LOGLEVEL``                    ``[logger_fietsboek] level``
=============================== ===============================

.. [#f1] Required.
.. [#f2] Defaults to a random string.

The ``fietsboek.pages`` setting is not exposed, as you can simply mount the
directory containing the pages to ``/fietsboek/pages``.

Upgrading
---------

The image is configured to automatically run ``fietsupdate`` at the start.
Therefore, simply updating the image to the new Fietsboek version should work
and the container will automatically migrate the data:

.. code-block:: bash

    # ... assume we checked out the new code, build the new image:
    docker build -t fietsboek .
    # Run the new code, taking the data from the old version:
    docker run --volumes-from=OLD_CONTAINER_ID -e ... fietsboek

.. warning::

    As always, back up your data before doing an update!

Maintenance & Cronjobs
----------------------

The image contains the maintenance tools ``fietsctl`` and ``fietscron``. You
can execute them in the container:

.. code-block:: bash

    # Get a user list ...
    docker exec -ti CONTAINER_ID fietsctl userlist
    # Run the cronjobs ...
    docker exec -ti CONTAINER_ID fietscron
    # ... and so on

Docker Compose
--------------

Since Fietsboek needs other services (such as a Redis_ instance, and optionally
a SQL server like PostgreSQL_ or MariaDB_), it makes sense to put those
services together in a ``docker-compose.yml`` file.

A minimal example is given here:

.. code-block:: yaml

    services:
      redis:
        image: redis

      fietsboek:
        image: fietsboek
        ports:
          - "8000:8000"
        environment:
          - REDIS_URL=redis://redis
        volumes:
          - fietsboek-data:/fietsboek/data
          - fietsboek-database:/fietsboek/database

    volumes:
      fietsboek-data:
      fietsboek-database:


This example shows how to use the environment-based configuration mechanism to
get a instance up quickly, without changing the defaults too much.

.. _Redis: https://redis.com/
.. _PostgreSQL: https://www.postgresql.org/
.. _MariaDB: https://mariadb.org/
