.. Fietsboek documentation master file, created by
   sphinx-quickstart on Thu Jun 30 20:46:28 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Fietsboek's documentation!
=====================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    administration
    developer
    user
    man

.. toctree::
    :maxdepth: 1

    changelog
    Website <https://fietsboek.org>
    Repository <https://gitlab.com/dunj3/fietsboek>

Fietsboek is a self-hostable sharing site for GPX track recordings with social
features. The goal is to have an application like `MyTourbook
<https://mytourbook.sourceforge.io/mytourbook/>`__ that runs as a web-service
and allows sharing and discovering of new tracks.

This documentation contains content pertaining to the installation, maintenance
and administration of a Fietsboek setup. Documentation about using Fietsboek
and extending/modifying Fietsboek will follow in the future.

Fietsboek is `FLOSS
<https://en.wikipedia.org/wiki/Free_and_open-source_software>`__, licensed
under the `AGPLv3 license <https://www.gnu.org/licenses/agpl-3.0.en.html>`__.
Fietsboek is built on top of the following open source technologies:

* `Pyramid <https://trypyramid.com/>`__
* `gpxpy <https://pypi.org/project/gpxpy/>`__
* Jürgen Berkemeier's `GPXViewer <https://www.j-berkemeier.de/GPXViewer/>`__
* `OpenStreetMap <https://www.openstreetmap.org/>`__
* `Bootstrap <https://getbootstrap.com/>`__ and `Bootstrap Icons
  <https://icons.getbootstrap.com/>`__
* `OpenSans <https://github.com/googlefonts/opensans>`__ (Copyright 2020 The
  Open Sans Project Authors)

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
