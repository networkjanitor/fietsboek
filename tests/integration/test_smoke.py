def test_home(testapp):
    res = testapp.get("/")
    assert res.status_code == 200
    assert b'<h1 class="float-start">Home</h1>' in res.body


def test_maintenance(testapp, data_manager):
    # Enable the maintenance mode:
    (data_manager.data_dir / "MAINTENANCE").write_text("Abbruch, abbruch!")

    try:
        res = testapp.get("/", status=503)
        assert b"Abbruch, abbruch" in res.body
    finally:
        # Make sure we disable maintenance mode for the following tests
        (data_manager.data_dir / "MAINTENANCE").unlink()
