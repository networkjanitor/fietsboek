Maintenance Mode
================

Sometimes, it can be useful to deactivate your fietsboek instance temporarily,
for example when you want to apply updates, or you need to otherwise fiddle
with the data. Deactivating your instance avoids any "race issues" or accesses
to the database and such, which might be in inconsistent states.

To help with this situation, fietsboek provides a *maintenance mode*. This
allows you to deactivate fietsboek without having to change with your webserver
settings.

In the maintenance mode, fietsboek will reply to any HTTP request with a ``503
Service Unavailable`` response, and won't process the requests further.

.. note::

    The maintenance mode is implemented early in fietsboek, however if you have
    custom HTTP proxies, caches or other WSGI middleware between fietsboek and
    the outside world, the maintenance mode will not affect those.

    In those cases, it might be better to deactivate the whole pipeline,
    depending on the setup and the middleware used — at least if those also
    access the database.

In addition to not replying to HTTP requests, ``fietscron`` will also respect
the maintenance mode and do nothing. This is to avoid accidental interference
with long tasks like updating, such that a badly-timed cronjob doesn't access
data in an invalid state.

Unlike ``fietscron`` and the web interface, ``fietsctl`` and ``fietsupdate``
will **continue to work**.

Controlling the Maintenance Mode
--------------------------------

You can enable and disable the maintenance mode using the ``fietsctl`` script.
To enable maintenance, simply pass a reason (that will be shown to users) to
``fietsctl maintenance-mode``::

    fietsctl -c production.ini maintenance-mode "Updating the instance"

Similarly, you can disable the maintenance mode by passing the ``--disable``
flag::

    fietsctl -c production.ini maintenance-mode --disable

To check the current status of the maintenance mode, call the subcommand
without extra arguments::

    fietsctl -c production.ini maintenance-mode

Manually Intervening with Maintenance
-------------------------------------

If you cannot (or don't want to) use the ``fietsctl maintenance-mode`` command,
you can also manually control the maintenance mode: The :file:`MAINTENANCE`
file in the data directory controls whether the mode is enabled or disabled.

To enable the maintenance mode, it is enough to ensure that the file exists::

    touch data-dir/MAINTENANCE

The reason for the maintenance mode is saved in the file content::

    echo "Updating the instance" > data-dir/MAINTENANCE

To disable the maintenance mode, remove the file again::

    rm data-dir/MAINTENANCE
