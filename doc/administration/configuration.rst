Configuration
=============

The main configuration of Fietsboek is done via ``.ini``-files. By default,
three such files exist, and one will be loaded at a time:

* ``production.ini`` contains the configuration for the production environment.
  It turns off debugging features (as they are a security risk!) and should
  contain the URL of the production database. This is the main file you want to
  use if you just want to deploy Fietsboek.
* ``development.ini`` contains the configuration for local development on
  Fietsboek. **This should not be used for production purposes, as it provides
  debugging information that poses a security risk!**
* ``testing.ini`` contains the configuration that the automated tests will use.

Most of the configuration is in the ``[app:main]`` category and looks like this:

.. code:: ini

    [app:main]
    use = egg:fietsboek

    pyramid.reload_templates = false
    pyramid.debug_authorization = false
    pyramid.debug_notfound = false
    pyramid.debug_routematch = false
    pyramid.default_locale_name = en

    email.from = fietsboek@localhost
    email.smtp_url = debug://localhost:1025

    available_locales = en de
    enable_account_registration = true

    session_key = <EDIT THIS>

    sqlalchemy.url = sqlite:///%(here)s/fietsboek.sqlite
    fietsboek.data_dir = %(here)s/data
    redis.url = redis://localhost/

    retry.attempts = 3

General Settings
----------------
Use ``enable_account_registration`` to enable or disable the creation of new
accounts via the web interface, for example if you want to have a private
instance. New accounts can always be created using the CLI management tool.

Set ``session_key`` to a random string of characters. This is the key used to
sign session data, so it should not get into wrong hands!

Use ``fietsboek.enable_image_uploads`` to enable or disable image uploads. By
default, track uploaders can add images to the track. Set this setting to
``false`` to disable this feature.

You can set up custom pages using ``fietsboek.pages``. See :doc:`custom-pages`
for more information.

Pyramid Settings
----------------

You should leave the ``use``, ``pyramid.reload_templates`` and
``pyramid.debug_*`` settings as they are. Refer to the `Pyramid documentation
<https://docs.pylonsproject.org/projects/pyramid/en/latest/narr/environment.html>`__
for more information.

Language Settings
-----------------

You can set the default language with the ``pyramid.default_locale_name``
setting. Note that Fietsboek will try to detect the user's language, so the
``default_locale_name`` is used as a fallback.

You can use ``available_locales`` to set the list of available languages.
Currently, Fietsboek ships with English ("en") and German ("de"). Removing a
language from this list will make it unavailable. If you create a custom
language locally, make sure to add it to this list here!

Fietsboek also allows you to install "language packs", providing languages from
third-party sources. Language packs are normal Python packages that must be
installed via the package manager (e.g. by using ``pip`` in the same
environment that you installed Fietsboek in), and then their names can be
listed as ``fietsboek.language_packs`` in the configuration. Note that you must
still add the locales to ``available_locales`` for them to work.

.. warning::

    Since language packs are just Python packages, they can contain and execute
    arbitrary code. Do not install untrusted language packs.

Database Settings
-----------------

Fietsboek uses three different databases:
A SQL database for persistent data (like user accounts), a file storage on the
disk for big files (like GPX files and images), and a redis server for
ephemeral data (like cached tiles).

Set ``sqlalchemy.url`` to the URL of the SQL database. See the `SQLAlchemy
documentation
<https://docs.sqlalchemy.org/en/14/core/engines.html#database-urls>`__ for more
information on available URL formats. Make sure to install the driver necessary
to communicate with your database (e.g. ``psycopg2`` for PostreSQL)!

Set ``fietsboek.data_dir`` to the directory for data uploads. This directory
must be writable by the Fietsboek process, as Fietsboek will save track data in
there.

Set ``redis.url`` to the URL of the redis instance. See the `redis module
documentation
<https://redis.readthedocs.io/en/latest/connections.html#redis.Redis.from_url>`__
for information about the possible syntaxes of this URL. Note that the redis
server is only used for caching and temporary data, so don't sweat to make it
persistent. A container running redis is fine.

.. note::

    Fietsboek will cache map tiles in the redis server.
    To avoid using up too much memory, consider setting a maximum memory size
    and policy in redis:

    https://redis.io/docs/management/config/#configuring-redis-as-a-cache

Email Settings
--------------

Use ``email.from`` to set the sender of emails, for example for account verifications.

Set ``email.smtp_url`` to the URL of the SMTP server. The following formats are
accepted:

* ``debug://`` a debug implementation that simply prints emails to the
  standard output. Should not be used in production, as no emails would ever
  arrive.
* ``smtp://host:port`` use the given SMTP server (without transport encryption!)
* ``smtp+ssl://host:port`` use the given SMTP server over a SSL connection.
* ``smtp+starttls://host:port`` use the given SMTP server and the STARTTLS
  command to start an encrypted channel.

Use ``email.username`` and ``email.password`` to set the login credentials for
the SMTP server.

Map Layers & Thunderforest Integration
--------------------------------------

By default, Fietsboek offers the following map layers:

* ``osm``: `OpenStreetMap <https://www.openstreetmap.org>`__
* ``osmde``: `OpenStreetMap Deutschland <https://www.openstreetmap.de/>`__
* ``satellite``: Satellite imaging from `Esri <https://www.esri.com>`__
* ``opentopo``: `OpenTopoMap <https://opentopomap.org/>`__
* ``topplusopen``: `TopPlus-Open
  <https://www.bkg.bund.de/SharedDocs/Produktinformationen/BKG/EN/P-2017/171114-TopPlus-Web-Open.html>`__

As well as the following overlay layers:

* ``opensea``: `OpenSeaMap <https://openseamap.org>`__
* ``cycling``: `Waymarked Trails: Cycling <https://cycling.waymarkedtrails.org>`__
* ``hiking``: `Waymarked Trails: Hiking <https://hiking.waymarkedtrails.org/>`__

You can use ``fietsboek.default_tile_layers`` to set the list of activated
layers (by default, all of them), for example:

.. code:: ini

    fietsboek.default_tile_layers = osm osmde cycling

You can enable `Thunderforest <https://www.thunderforest.com>`__ support by
setting ``thunderforest.api_key``, and ``thunderforest.maps`` to a list of
Thunderforest maps (e.g. "cycle" or "landscape"). By default, only logged in
users will be able to use the Thunderforest maps (to protect your quota), this
can be changed by setting ``thunderforest.access = public`` (default is
"restricted").

You can enable `Stamen <http://maps.stamen.com>`__ support by setting
``stamen.maps`` to the desired maps, e.g. ``stamen.maps = toner terrain
watercolor``.

You can add custom tile layers in the following way:

.. code:: ini

    fietsboek.tile_layer.ID = My Custom Layer
    fietsboek.tile_layer.ID.url = https://tiles.example.com/{z}/{x}/{y}.png
    # Optional, set the type (base or overlay), default base
    fietsboek.tile_layer.ID.type = base
    # Optional, set the maximum zoom factor, default 22
    fietsboek.tile_layer.ID.zoom = 22
    # Optional, set the attribution
    fietsboek.tile_layer.ID.attribution = Copyright Example
    # Optional, set the access restriction (public or restricted), default
    # public
    fietsboek.tile_layer.ID.access = public

``ID`` must be an alphanumerical identifier of your choosing.

By default, Fietsboek will proxy all tile requests through the Fietsboek
instance. While this can slow down the user experience and increase the load on
your server, it has the following benefits:

* Your users' IPs stay private and protected, as no third party is contacted.
  The tile servers will only see the IP from the Fietsboek server.
* If you use private tile servers or servers that require a key, your key is
  protected as it will not be given out to the users.
* Fietsboek caches tile requests, which reduces the strain on the providers and
  might even make maps faster if many people use them.

You can disable the tile proxy by setting ``fietsboek.tile_proxy.disable =
true``. This will cause the tiles to be loaded directly by the client.

.. warning::

    If you disable the tile proxy, all tile source URLs will be given to the
    user. If you use API keys or other private sources, **those keys will be
    leaked to the users**.

    In addition, depending on the jurisdiction, you might be required to tell
    your users that third party content is included in your site, and that
    their IP will be accessible to the third party.

Hittekaart Integration
----------------------

Fietsboek can use hittekaart_ to generate heat maps for users. For that, you
can set ``hittekaart.bin`` to the path to the ``hittekaart`` binary. If unset,
it is assumed that the binary can be found in your ``$PATH``.

In addition, you can set ``hittekaart.autogenerate`` to the list of overlay
maps you want to automatically generate and update. By default, this list is
empty, which means that Fietsboek will not generate any overlays on its own.
You can add ``heatmap`` and/or ``tilehunter`` to generate those maps
automatically.

By default, ``hittekaart`` will use as many threads as CPU cores are available.
This leads to the fastest heatmap generation, but might be undesired on shared
hosts that also have other services running. You can explicitely set a thread
count by setting ``hittekaart.threads``. A value of "0" is equivalent to the
default behavior.

.. note::

    The ``hittekaart.autogenerate`` setting has no effect on the ``fietsctl
    hittekaart`` command. You can always use ``fietsctl`` to generate heat maps
    manually!

.. warning::

    Depending on the geospatial area that a user covers with their tracks, an
    overlay map can get relatively large (in my case ~100 MiB). Keep that in
    mind when hosting a larger number of users.

An example configuration excerpt can look like this:

.. code-block:: ini

    hittekaart.bin = /usr/local/bin/hittekaart
    hittekaart.autogenerate = heatmap tilehunter
    hittekaart.threads = 2

.. _hittekaart: https://gitlab.com/dunj3/hittekaart
