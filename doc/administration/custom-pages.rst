Custom Pages
############

Sometimes it is necessary to add custom static content to your website. For
example, a legal requirement might be to have an `Impressum
<https://en.wikipedia.org/wiki/Impressum>`__, a privacy policy, contact
information or similar things on a website. Or you simply want to add some
information about your instance of Fietsboek, links to other sites, ...

Such pages can not be provided by Fietsboek out of the box, as they are very
tailored to the particular site. However, Fietsboek provides a way to include
custom static pages. Those pages have the benefit of being embedded in
Fietsboek's menu and layout, so they don't look out of place and can easily be
found.

.. note::

    Please note that Fietsboek is not a general purpose content management
    system for text. As such, the functionality is rather rudimentary and meant
    for basic tasks such as the aforementioned legal documents.

    Complex documents can always be done outside of Fietsboek, or by modifying
    Fietsboek's source in your local installation.

Writing A Page
--------------

Pages are written in Markdown and as such support some basic formatting
functionality [1]_. In addition to the content, a page also has some metadata
that tell Fietsboek when to show it, where to place it in the menu, ...

An example page could look like this:

.. code:: markdown

    Title: Open Source
    Link-name: Open Source
    Slug: open-source
    Locale: en
    Show-to: everyone
    Index: 1

    # Fietsboek is Open Source!

    You can contribute to **Fietsboek** [on Gitlab](https://gitlab.com/dunj3/fietsboek)!

The metadata is provided in form of ``Key: value`` attributes at the start of
the file. The rest of the file is interpreted as Markdown and rendered to HTML.

The supported attributes are:

Title : required
    The title of the page, as it should be shown in the title bar of the browser.

Link-name : required
    The name of the link, as it is rendered in the menu.

Slug : required
    The slug of the page, as it appears in the URL. The page is reachable under
    ``https://{your-host}/page/{page-slug}``.

Locale : optional
    Optional filter for the page locale. The filter is given as a regular
    expression, the page is only shown if the expression matches the user's
    locale. Multiple locale filters can be given, in which case any of them
    have to match in order for the page to be shown.

Show-to : optional
    Determines whether the page should be shown to everyone (``everyone``),
    only to logged in users (``logged-in``) or only to logged out users
    (``logged-out``).

Index : optional
    Determines the position of the item in the menu. A higher index means the
    item is more to the right. Pages with negative index are left of
    Fietsboek's own menu, pages with positive index are right of Fietsboek's
    own menu, and pages with index 0 (the default) are not rendered in the menu
    (but still accessible via the link).

The source code of a page (including the metadata and attributes) should be
saved at a path that is accessible by Fietsboek.


Including Pages
---------------

After you have written and saved the page, you can include it via the
configuration file. The key ``fietsboek.pages`` is a list of paths to pages:

.. code:: ini

    [app:main]
    # ...
    fietsboek.pages =
        /fietsboek/pages/page1.md
        /fietsboek/pages/page2.md

You can also include a directory, in which case all ``.md`` files of that
directory will be included:

.. code:: ini

    [app:main]
    # ...
    fietsboek.pages =
        /fietsboek/pages/

Overwriting the Home Page
-------------------------

If you set a slug of ``/``, you can overwrite Fietsboek's custom home page:

.. code:: markdown

    Title: Home
    Slug: /
    Link-name: home

    Welcome to Fietsboek!
    =====================

Note that you can use the same ``Locale`` and ``Show-to`` filters for the home
page as you can for other custom pages. If no matching home page is found,
Fietsboek will fall back to the default.

When defining your own home page, make sure to still include it in the
configuration!

Tips & Tricks
-------------

You can use the ``Locale`` filter to provide localized versions of your pages.
If you keep the ``Slug`` the same between different languages, you can even
re-use the same URL!

.. [1] A basic syntax overview is for example here:
   https://www.markdownguide.org/basic-syntax/
