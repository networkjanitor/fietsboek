module.exports = {
  extends: ['eslint:recommended', 'plugin:@typescript-eslint/recommended'],
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  rules: {
      "@typescript-eslint/no-non-null-assertion": "off",
      "@typescript-eslint/no-unused-vars": [1, {
          varsIgnorePattern: "^_",
          argsIgnorePattern: "^_",
      }],
      "@typescript-eslint/no-explicit-any": "off",
      "max-len": [2, 100],
      "semi": [2, "always"],
      "comma-dangle": [2, "always-multiline"],
  },
  root: true,
};
